<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_scrum_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_scrum';
  $context->description = 'Keeps the \'Scrum\' menu active as needed';
  $context->tag = 'Atrium Scrum';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'atrium_scrum' => 'atrium_scrum',
        'atrium_scrum_backlog' => 'atrium_scrum_backlog',
        'atrium_scrum_burndown' => 'atrium_scrum_burndown',
        'atrium_scrum_sprint' => 'atrium_scrum_sprint',
        'scrum_tasks_task_board' => 'scrum_tasks_task_board',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'scrum',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium Scrum');
  t('Keeps the \'Scrum\' menu active as needed');

  $export['atrium_scrum'] = $context;
  return $export;
}
