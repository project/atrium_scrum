<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_scrum_sprints_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_scrum_sprints_node_info() {
  $items = array(
    'sprint' => array(
      'name' => t('Sprint'),
      'module' => 'features',
      'description' => t('A Scrum Sprint consists of a fixed duration (typically, 2-4 weeks), during which the Scrum Team undertakes a fixed amount of work, the Sprint Backlog, to achieve the Sprint Goal. (ref: <a href="http://www.scrumalliance.org/articles/39-glossary-of-scrum-terms#1118">Scrum Alliance</a> and <a href="http://en.wikipedia.org/wiki/Scrum_(development)#Sprint">Wikipedia</a>.)'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => t('A Scrum Sprint consists of a fixed duration (typically, 2-4 weeks), during which the Scrum Team undertakes a fixed amount of work, the Sprint Backlog, to achieve the Sprint Goal. (ref: <a href="http://www.scrumalliance.org/articles/39-glossary-of-scrum-terms#1118">Scrum Alliance</a> and <a href="http://en.wikipedia.org/wiki/Scrum_(development)#Sprint">Wikipedia</a>.)'),
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function atrium_scrum_sprints_views_api() {
  return array(
    'api' => '2',
  );
}
