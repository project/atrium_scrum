<?php

/**
 * Implementation of hook_default_corresponding_node_references_preset().
 */
function atrium_scrum_sprints_default_corresponding_node_references_preset() {
  $export = array();

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'sprint*field_scrum_sprint_backlog*scrum_task*field_scrum_sprint_reference';
  $preset->enabled = 1;
  $export['sprint*field_scrum_sprint_backlog*scrum_task*field_scrum_sprint_reference'] = $preset;

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'sprint*field_scrum_sprint_backlog*scrum_user_story*field_scrum_sprint_reference';
  $preset->enabled = 1;
  $export['sprint*field_scrum_sprint_backlog*scrum_user_story*field_scrum_sprint_reference'] = $preset;

  return $export;
}
