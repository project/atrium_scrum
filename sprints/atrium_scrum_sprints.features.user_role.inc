<?php

/**
 * Implementation of hook_user_default_roles().
 */
function atrium_scrum_sprints_user_default_roles() {
  $roles = array();

  // Exported role: Scrum Master
  $roles['Scrum Master'] = array(
    'name' => 'Scrum Master',
  );

  return $roles;
}
