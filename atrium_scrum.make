core = 6.x
api = 2

; Contrib modules

projects[cnr][subdir] = "unstable"
projects[cnr][type] = "module"
projects[cnr][download][type] = "git"
projects[cnr][download][url] = "http://git.drupal.org/project/cnr.git"
projects[cnr][download][branch] = "6.x-4.x"
; ref.: http://drupal.org/node/1213898
projects[cnr][patch][] = "http://drupal.org/files/cnr-backport_exportables_patch-1213898-18.patch"
; ref.: http://drupal.org/node/1434138
projects[cnr][patch][] = "http://drupal.org/files/more_helpful_error_message.patch"

; ref.: http://drupal.org/node/514836#comment-3051044
projects[views_customfield][subdir] = "unstable"
projects[views_customfield][version] = "1.x-dev"

; ref.: http://drupal.org/node/917270
; since we're overriding the version in the profile, this will likely need to
; in a stub makefile
projects[itweak_upload][subdir] = "unstable"
projects[itweak_upload][version] = "2.x-dev"

projects[comment_driven][subdir] = "contrib"
projects[comment_driven][version] = "1.0-alpha3"

projects[driven][subdir] = "contrib"
projects[driven][version] = "1.0-alpha3"

projects[views_field_view][subdir] = "contrib"
projects[views_field_view][version] = "1.0-beta1"

projects[views_fluid_grid][subdir] = "contrib"
projects[views_fluid_grid][version] = "1.1"

projects[views_arg_context][subdir] = "contrib"
projects[views_arg_context][version] = "1.3"

projects[automodal][subdir] = "contrib"
projects[automodal][version] = "1.0"

projects[dirtyforms][subdir] = "contrib"
projects[dirtyforms][version] = "1.1"

projects[onbeforeunload][subdir] = "contrib"
projects[onbeforeunload][version] = "1.0"

projects[modalframe_exclude_node][subdir] = "contrib"
projects[modalframe_exclude_node][version] = "1.0"

projects[jquery_ui][subdir] = "contrib"
projects[jquery_ui][version] = "1.5"

projects[modalframe][subdir] = "contrib"
projects[modalframe][version] = "1.7"

projects[auto_nodetitle][subdir] = "contrib"
projects[auto_nodetitle][version] = "1.2"

projects[computed_field][subdir] = "contrib"
projects[computed_field][version] = "1.0"

projects[hierarchical_select][subdir] = "contrib"
projects[hierarchical_select][version] = "3.8"

projects[draggableviews][subdir] = "contrib"
projects[draggableviews][version] = "3.5"

projects[views_bulk_operations][subdir] = "contrib"
projects[views_bulk_operations][version] = "1.13"

projects[charts_graphs][subdir] = "contrib"
projects[charts_graphs][version] = "2.7"

projects[views_charts][subdir] = "contrib"
projects[views_charts][version] = "1.1"
