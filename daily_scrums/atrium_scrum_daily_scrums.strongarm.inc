<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_scrum_daily_scrums_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'atrium_activity_update_type_daily_scrum';
  $strongarm->value = 0;

  $export['atrium_activity_update_type_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_daily_scrum';
  $strongarm->value = 0;

  $export['comment_anonymous_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_daily_scrum';
  $strongarm->value = '3';

  $export['comment_controls_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_daily_scrum';
  $strongarm->value = '0';

  $export['comment_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_daily_scrum';
  $strongarm->value = '4';

  $export['comment_default_mode_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_daily_scrum';
  $strongarm->value = '1';

  $export['comment_default_order_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_daily_scrum';
  $strongarm->value = '50';

  $export['comment_default_per_page_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_daily_scrum';
  $strongarm->value = '0';

  $export['comment_form_location_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_daily_scrum';
  $strongarm->value = '1';

  $export['comment_preview_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_daily_scrum';
  $strongarm->value = '1';

  $export['comment_subject_field_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_daily_scrum';
  $strongarm->value = array(
    'title' => '-5',
    'body_field' => '-2',
    'revision_information' => '4',
    'author' => '3',
    'options' => '5',
    'comment_settings' => '6',
    'menu' => '1',
    'book' => '0',
    'path' => '7',
    'og_nodeapi' => '2',
  );

  $export['content_extra_weights_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_daily_scrum';
  $strongarm->value = 1;

  $export['enable_revisions_page_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'modalframe_exclude_node_daily_scrum';
  $strongarm->value = array();

  $export['modalframe_exclude_node_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_daily_scrum';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );

  $export['node_options_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_daily_scrum';
  $strongarm->value = 0;

  $export['show_diff_inline_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_daily_scrum';
  $strongarm->value = 0;

  $export['show_preview_changes_daily_scrum'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_daily_scrum';
  $strongarm->value = '0';

  $export['upload_daily_scrum'] = $strongarm;
  return $export;
}
