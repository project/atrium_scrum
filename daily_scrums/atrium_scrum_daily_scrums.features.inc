<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_scrum_daily_scrums_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_scrum_daily_scrums_node_info() {
  $items = array(
    'daily_scrum' => array(
      'name' => t('Daily Scrum'),
      'module' => 'features',
      'description' => t('A brief daily status meeting to highlight work done since the last scrum, work planned before the next one, and impediments to progress.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Notes'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}
