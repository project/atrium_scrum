<?php

/**
 * Implementation of hook_default_corresponding_node_references_preset().
 */
function atrium_scrum_daily_scrums_default_corresponding_node_references_preset() {
  $export = array();
  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'daily_scrum*field_scrum_sprint_reference*sprint*field_scrum_daily_scrum_days';
  $preset->enabled = 1;

  $export['daily_scrum*field_scrum_sprint_reference*sprint*field_scrum_daily_scrum_days'] = $preset;
  return $export;
}
