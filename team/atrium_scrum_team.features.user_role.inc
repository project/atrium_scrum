<?php

/**
 * Implementation of hook_user_default_roles().
 */
function atrium_scrum_team_user_default_roles() {
  $roles = array();

  // Exported role: Scrum Team Member
  $roles['Scrum Team Member'] = array(
    'name' => 'Scrum Team Member',
  );

  return $roles;
}
