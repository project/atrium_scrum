<?php

/**
 * Implementation of hook_default_corresponding_node_references_preset().
 */
function atrium_scrum_team_default_corresponding_node_references_preset() {
  $export = array();

  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'profile*field_scrum_team_sprints*sprint*field_scrum_sprint_team';
  $preset->enabled = 1;
  $export['profile*field_scrum_team_sprints*sprint*field_scrum_sprint_team'] = $preset;

  return $export;
}
