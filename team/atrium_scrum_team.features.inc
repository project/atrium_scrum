<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_scrum_team_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_views_api().
 */
function atrium_scrum_team_views_api() {
  return array(
    'api' => '2',
  );
}
