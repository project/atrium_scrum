<?php

/**
 * Implementation of hook_content_default_fields().
 */
function atrium_scrum_team_content_default_fields() {
  $fields = array();

  // Exported field: field_availability
  $fields['profile-field_availability'] = array(
    'field_name' => 'field_availability',
    'type_name' => 'profile',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_decimal',
    'required' => '0',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => ' days/week',
    'min' => '',
    'max' => '7',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'precision' => '10',
    'scale' => '2',
    'decimal' => '.',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_availability][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Availability',
      'weight' => '-4',
      'description' => 'How many days per week are you available to work on Scrum Sprint projects?
This is the total sum of your time available, so if you can work 5 half days, you would enter \'2.5\'',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_scrum_team_sprints
  $fields['profile-field_scrum_team_sprints'] = array(
    'field_name' => 'field_scrum_team_sprints',
    'type_name' => 'profile',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'sprint' => 'sprint',
      'blog' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'daily_scrum' => 0,
      'event' => 0,
      'group' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'scrum_task' => 0,
      'scrum_user_story' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_scrum_team_sprints][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Scrum Sprint Membership',
      'weight' => '-3',
      'description' => 'What Scrum Sprints are you participating in as a Team Member?
This field is for reference purposes only and is thus read-only. Sprint membership is managed via the Sprint form.',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_scrum_sprint_team
  $fields['sprint-field_scrum_sprint_team'] = array(
    'field_name' => 'field_scrum_sprint_team',
    'type_name' => 'sprint',
    'display_settings' => array(
      'label' => array(
        'format' => 'above',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'profile' => 'profile',
      'blog' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'daily_scrum' => 0,
      'event' => 0,
      'group' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'sprint' => 0,
      'scrum_task' => 0,
      'scrum_user_story' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => 'scrum_team_members',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Scrum Team',
      'weight' => 0,
      'description' => 'The team responsible for delivering product increments at the end of each Sprint. (ref.: <a href="http://www.scrumalliance.org/articles/39-glossary-of-scrum-terms#1112">Scrum Alliance</a>.)123',
      'type' => 'nodereference_buttons',
      'module' => 'nodereference',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Availability');
  t('Scrum Sprint Membership');
  t('Scrum Team');

  return $fields;
}
