<?php

/**
 * Implementation of hook_views_default_views().
 */
function atrium_scrum_burndown_views_default_views() {
  $views = array();

  // Exported view: atrium_scrum_burndown
  $view = new view;
  $view->name = 'atrium_scrum_burndown';
  $view->description = 'Adds Burndown Charts so we can measure progress throughout the sprint';
  $view->tag = 'atrium_scrum';
  $view->view_php = '';
  $view->base_table = 'node_revisions';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('relationships', array(
    'field_scrum_daily_scrum_days_nid' => array(
      'label' => 'Daily Scrums',
      'required' => 0,
      'delta' => '-1',
      'id' => 'field_scrum_daily_scrum_days_nid',
      'table' => 'node_data_field_scrum_daily_scrum_days',
      'field' => 'field_scrum_daily_scrum_days_nid',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('fields', array(
    'field_scrum_sprint_duration_value' => array(
      'label' => 'Duration',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_scrum_sprint_duration_value',
      'table' => 'node_data_field_scrum_sprint_duration',
      'field' => 'field_scrum_sprint_duration_value',
      'relationship' => 'field_sprint_reference_nid',
    ),
    'field_scrum_sprint_reqd_effort_value' => array(
      'label' => 'Remaining Effort',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'custom',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_scrum_sprint_reqd_effort_value',
      'table' => 'node_data_field_scrum_sprint_reqd_effort',
      'field' => 'field_scrum_sprint_reqd_effort_value',
      'relationship' => 'none',
    ),
    'field_scrum_daily_scrum_day_value' => array(
      'label' => 'Day',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_scrum_daily_scrum_day_value',
      'table' => 'node_data_field_scrum_daily_scrum_day',
      'field' => 'field_scrum_daily_scrum_day_value',
      'relationship' => 'field_scrum_daily_scrum_days_nid',
    ),
    'phpcode_1' => array(
      'label' => 'Ideal Velocity',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'value' => '<?php
//module_load_include(\'module\', \'atrium_scrum_burndown\');
//dsm(atrium_scrum_burndown_ideal_velocity($data));
//return (int) atrium_scrum_burndown_ideal_velocity($data);
  $node = node_load($data->node_nid);
  $reqd_effort = $node->field_scrum_sprint_reqd_effort[0][\'value\'];
  $sprint_duration = isset($data->node_data_field_scrum_sprint_duration_field_scrum_sprint_duration_value) ? $data->node_data_field_scrum_sprint_duration_field_scrum_sprint_duration_value : $node->field_scrum_sprint_duration[0][\'value\'];
  $day = $data->node_node_data_field_scrum_daily_scrum_days_node_data_field_scrum_daily_scrum_day_field_scrum_daily_scrum_day_value;
  return round($reqd_effort - ($reqd_effort / $sprint_duration) * $day);
?>',
      'exclude' => 0,
      'sortable' => '0',
      'id' => 'phpcode_1',
      'table' => 'customfield',
      'field' => 'phpcode',
      'relationship' => 'none',
      'override' => array(
        'button' => 'Override',
      ),
    ),
    'field_scrum_sprint_avail_effort_value' => array(
      'label' => 'Available Effort',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'link_to_node' => 0,
      'label_type' => 'widget',
      'format' => 'default',
      'multiple' => array(
        'group' => TRUE,
        'multiple_number' => '',
        'multiple_from' => '',
        'multiple_reversed' => FALSE,
      ),
      'exclude' => 0,
      'id' => 'field_scrum_sprint_avail_effort_value',
      'table' => 'node_data_field_scrum_sprint_avail_effort',
      'field' => 'field_scrum_sprint_avail_effort_value',
      'relationship' => 'field_sprint_reference_nid',
    ),
    'phpcode' => array(
      'label' => 'Available Velocity',
      'alter' => array(
        'alter_text' => 0,
        'text' => '',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'value' => '<?php
module_load_include(\'module\', \'atrium_scrum_burndown\');
return (int) atrium_scrum_burndown_available_velocity($data);
?>',
      'sortable' => '0',
      'exclude' => 0,
      'id' => 'phpcode',
      'table' => 'customfield',
      'field' => 'phpcode',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('sorts', array(
    'timestamp' => array(
      'order' => 'ASC',
      'granularity' => 'second',
      'id' => 'timestamp',
      'table' => 'node_revisions',
      'field' => 'timestamp',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('arguments', array(
    'nid' => array(
      'default_action' => 'default',
      'style_plugin' => 'default_summary',
      'style_options' => array(),
      'wildcard' => 'all',
      'wildcard_substitution' => 'All',
      'title' => '',
      'breadcrumb' => '',
      'default_argument_type' => 'context',
      'default_argument' => '',
      'validate_type' => 'all',
      'validate_fail' => 'not found',
      'break_phrase' => 0,
      'not' => 0,
      'id' => 'nid',
      'table' => 'node',
      'field' => 'nid',
      'validate_user_argument_type' => 'uid',
      'validate_user_roles' => array(
        '2' => 0,
        '3' => 0,
        '4' => 0,
        '5' => 0,
        '6' => 0,
        '7' => 0,
      ),
      'override' => array(
        'button' => 'Override',
      ),
      'relationship' => 'none',
      'default_options_div_prefix' => '',
      'default_argument_fixed' => '',
      'default_argument_user' => 0,
      'default_argument_php' => 'return arg(2);',
      'validate_argument_node_type' => array(
        'blog' => 0,
        'book' => 0,
        'event' => 0,
        'feed_ical' => 0,
        'feed_ical_item' => 0,
        'group' => 0,
        'profile' => 0,
        'shoutbox' => 0,
        'sprint' => 0,
        'casetracker_basic_case' => 0,
        'casetracker_basic_project' => 0,
      ),
      'validate_argument_node_access' => 0,
      'validate_argument_nid_type' => 'nid',
      'validate_argument_vocabulary' => array(
        '1' => 0,
      ),
      'validate_argument_type' => 'tid',
      'validate_argument_transform' => 0,
      'validate_user_restrict_roles' => 0,
      'validate_argument_is_member' => 'OG_VIEWS_DO_NOT_VALIDATE_MEMBERSHIP',
      'validate_argument_group_node_type' => array(
        'group' => 0,
      ),
      'validate_argument_php' => '',
      'namespace' => 'atrium_scrum_sprint',
      'attribute' => 'current_sprint_nid',
    ),
  ));
  $handler->override_option('filters', array(
    'type' => array(
      'operator' => 'in',
      'value' => array(
        'sprint' => 'sprint',
      ),
      'group' => '0',
      'exposed' => FALSE,
      'expose' => array(
        'operator' => FALSE,
        'label' => '',
      ),
      'id' => 'type',
      'table' => 'node',
      'field' => 'type',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'spaces_feature',
    'spaces_feature' => 'atrium_scrum_burndown',
    'perm' => 'access content',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Burndown Chart');
  $handler->override_option('items_per_page', 0);
  $handler->override_option('style_plugin', 'table');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'override' => 1,
    'sticky' => 0,
    'order' => 'asc',
    'columns' => array(
      'counter' => 'counter',
      'rownumber' => 'rownumber',
      'nid' => 'nid',
      'timestamp' => 'timestamp',
      'vid' => 'vid',
      'field_effort_value' => 'field_effort_value',
      'field_scrum_sprint_duration_value' => 'field_scrum_sprint_duration_value',
      'field_scrum_sprint_reqd_effort_value' => 'field_scrum_sprint_reqd_effort_value',
      'field_scrum_sprint_dates_value' => 'field_scrum_sprint_dates_value',
      'field_scrum_sprint_dates_value2' => 'field_scrum_sprint_dates_value2',
    ),
    'info' => array(
      'counter' => array(
        'separator' => '',
      ),
      'rownumber' => array(
        'separator' => '',
      ),
      'nid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'timestamp' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'vid' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_effort_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_scrum_sprint_duration_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_scrum_sprint_reqd_effort_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_scrum_sprint_dates_value' => array(
        'sortable' => 0,
        'separator' => '',
      ),
      'field_scrum_sprint_dates_value2' => array(
        'sortable' => 0,
        'separator' => '',
      ),
    ),
    'default' => '-1',
  ));
  $handler = $view->new_display('page', 'Burndown Chart Page', 'page_1');
  $handler->override_option('style_plugin', 'charts');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'views_charts_series_fields' => array(
      'field_scrum_sprint_reqd_effort_value' => 'field_scrum_sprint_reqd_effort_value',
      'phpcode_1' => 'phpcode_1',
      'phpcode' => 'phpcode',
    ),
    'views_charts_x_labels' => 'field_scrum_daily_scrum_day_value',
    'width' => '600',
    'height' => '400',
    'engine' => 'google-charts',
    'type' => array(
      'google-charts' => 'line',
    ),
    'wmode' => 'window',
    'y_min' => '',
    'y_max' => '',
    'y_step' => '',
    'y_legend' => '',
    'background_colour' => '',
    'series_colours' => '',
  ));
  $handler->override_option('path', 'scrum/sprints/%/burndown');
  $handler->override_option('menu', array(
    'type' => 'tab',
    'title' => 'Burndown Chart',
    'description' => '',
    'weight' => '1',
    'name' => 'navigation',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'none',
    'title' => '',
    'description' => '',
    'weight' => 0,
    'name' => 'navigation',
  ));
  $handler = $view->new_display('block', 'Burndown Chart Block', 'block_1');
  $handler->override_option('use_more', 1);
  $handler->override_option('use_more_always', 1);
  $handler->override_option('use_more_text', 'larger graph');
  $handler->override_option('style_plugin', 'charts');
  $handler->override_option('style_options', array(
    'grouping' => '',
    'views_charts_series_fields' => array(
      'field_scrum_sprint_reqd_effort_value' => 'field_scrum_sprint_reqd_effort_value',
      'phpcode_1' => 'phpcode_1',
      'phpcode' => 'phpcode',
    ),
    'views_charts_x_labels' => 'phpcode_2',
    'width' => '300',
    'height' => '200',
    'engine' => 'google-charts',
    'type' => array(
      'google-charts' => 'line',
    ),
    'wmode' => 'window',
    'y_min' => '0.0',
    'y_max' => '',
    'y_step' => '',
    'y_legend' => '',
    'background_colour' => '',
    'series_colours' => '#FF0000, #00FF00, #0000FF',
  ));
  $handler->override_option('block_description', '');
  $handler->override_option('block_caching', -1);
  $translatables['atrium_scrum_burndown'] = array(
    t('Burndown Chart'),
    t('Burndown Chart Block'),
    t('Burndown Chart Page'),
    t('Defaults'),
    t('larger graph'),
  );

  $views[$view->name] = $view;

  return $views;
}
