<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_scrum_burndown_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_scrum_burndown';
  $context->description = 'Adds a burndown chart block to the detailed sprint page';
  $context->tag = 'Atrium Scrum';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'atrium_scrum_sprint:page_1' => 'atrium_scrum_sprint:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-atrium_scrum_burndown-block_1' => array(
          'module' => 'views',
          'delta' => 'atrium_scrum_burndown-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds a burndown chart block to the detailed sprint page');
  t('Atrium Scrum');

  $export['atrium_scrum_burndown'] = $context;
  return $export;
}
