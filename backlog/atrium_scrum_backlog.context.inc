<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_scrum_backlog_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_scrum_backlog';
  $context->description = 'Keeps the \'Scrum\' menu active when under the \'backlog\' tab';
  $context->tag = 'Atrium Scrum';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'atrium_scrum_backlog' => 'atrium_scrum_backlog',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'scrum',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium Scrum');
  t('Keeps the \'Scrum\' menu active when under the \'backlog\' tab');

  $export['atrium_scrum_backlog'] = $context;
  return $export;
}
