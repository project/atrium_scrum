<?php

/**
 * Implementation of hook_user_default_roles().
 */
function atrium_scrum_backlog_user_default_roles() {
  $roles = array();

  // Exported role: Product Owner
  $roles['Product Owner'] = array(
    'name' => 'Product Owner',
  );

  return $roles;
}
