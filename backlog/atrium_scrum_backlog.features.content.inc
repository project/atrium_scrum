<?php

/**
 * Implementation of hook_content_default_fields().
 */
function atrium_scrum_backlog_content_default_fields() {
  $fields = array();

  // Exported field: field_scrum_backlog_priority
  $fields['scrum_user_story-field_scrum_backlog_priority'] = array(
    'field_name' => 'field_scrum_backlog_priority',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '53',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '0',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => '',
    'min' => '',
    'max' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scrum_backlog_priority][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Backlog Priority',
      'weight' => '53',
      'description' => '',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Backlog Priority');

  return $fields;
}
