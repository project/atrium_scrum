<?php

/**
 * Implementation of hook_views_default_views().
 */
function atrium_scrum_views_default_views() {
  $views = array();

  // Exported view: atrium_scrum
  $view = new view;
  $view->name = 'atrium_scrum';
  $view->description = 'Adds an overview page and menu that other sub-features can hook into';
  $view->tag = 'atrium_scrum';
  $view->view_php = '';
  $view->base_table = 'node';
  $view->is_cacheable = FALSE;
  $view->api_version = 2;
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */
  $handler = $view->new_display('default', 'Defaults', 'default');
  $handler->override_option('fields', array(
    'nothing' => array(
      'label' => '',
      'alter' => array(
        'text' => 'This page contains basic information about the Scrum process',
        'make_link' => 0,
        'path' => '',
        'link_class' => '',
        'alt' => '',
        'prefix' => '',
        'suffix' => '',
        'target' => '',
        'help' => '',
        'trim' => 0,
        'max_length' => '',
        'word_boundary' => 1,
        'ellipsis' => 1,
        'html' => 0,
        'strip_tags' => 0,
      ),
      'empty' => '',
      'hide_empty' => 0,
      'empty_zero' => 0,
      'exclude' => 0,
      'id' => 'nothing',
      'table' => 'views',
      'field' => 'nothing',
      'relationship' => 'none',
    ),
  ));
  $handler->override_option('access', array(
    'type' => 'spaces_feature',
    'spaces_feature' => 'atrium_scrum',
    'perm' => '',
  ));
  $handler->override_option('cache', array(
    'type' => 'none',
  ));
  $handler->override_option('title', 'Scrum Overview');
  $handler->override_option('header', 'Scrum Overview');
  $handler->override_option('header_format', '5');
  $handler->override_option('header_empty', 1);
  $handler->override_option('items_per_page', 1);
  $handler = $view->new_display('page', 'Scrum Overview Page', 'page_1');
  $handler->override_option('path', 'scrum/filter');
  $handler->override_option('menu', array(
    'type' => 'default tab',
    'title' => 'Overview',
    'description' => '',
    'weight' => '-10',
    'name' => 'features',
  ));
  $handler->override_option('tab_options', array(
    'type' => 'normal',
    'title' => 'Scrum',
    'description' => '',
    'weight' => '0',
    'name' => 'features',
  ));
  $translatables['atrium_scrum'] = array(
    t('Defaults'),
    t('Scrum Overview'),
    t('Scrum Overview Page'),
  );

  $views[$view->name] = $view;

  return $views;
}
