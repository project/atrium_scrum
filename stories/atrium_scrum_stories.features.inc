<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_scrum_stories_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_scrum_stories_node_info() {
  $items = array(
    'scrum_user_story' => array(
      'name' => t('User Story'),
      'module' => 'features',
      'description' => t('A Scrum user story. May contain tasks or other user stories.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => t('A user story is a high-level definition of a requirement, containing just enough information so that the developers can produce a reasonable estimate of the effort to implement it.
(ref.: <a href="http://www.agilemodeling.com/artifacts/userStory.htm">Agile Modeling</a>)'),
    ),
  );
  return $items;
}
