<?php

/**
 * Implementation of hook_taxonomy_default_vocabularies().
 */
function atrium_scrum_stories_taxonomy_default_vocabularies() {
  return array(
    'atrium_scrum_stories_role' => array(
      'name' => 'As a',
      'description' => '',
      'help' => 'The role or roles that this User Story applies to, in a comma-separated list.',
      'relations' => '1',
      'hierarchy' => '0',
      'multiple' => '0',
      'required' => '1',
      'tags' => '1',
      'module' => 'features_atrium_scrum_stories_role',
      'weight' => '0',
      'nodes' => array(
        'scrum_user_story' => 'scrum_user_story',
      ),
    ),
  );
}
