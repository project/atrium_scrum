<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_scrum_stories_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_scrum_stories';
  $context->description = 'Adds \'Add User Story\' button';
  $context->tag = 'Atrium Scrum';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'scrum_user_story' => 'scrum_user_story',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'atrium_scrum' => 'atrium_scrum',
        'atrium_scrum_backlog' => 'atrium_scrum_backlog',
        'atrium_scrum_burndown' => 'atrium_scrum_burndown',
        'atrium_scrum_sprint' => 'atrium_scrum_sprint',
        'scrum_tasks_task_board' => 'scrum_tasks_task_board',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'scrum',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds \'Add User Story\' button');
  t('Atrium Scrum');
  $export['atrium_scrum_stories'] = $context;

  return $export;
}
