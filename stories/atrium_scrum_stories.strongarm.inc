<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_scrum_stories_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_pattern_scrum_user_story';
  $strongarm->value = '<?php
  return ucfirst($node->field_scrum_stories_iwant[0][\'value\']);
?>';
  $export['ant_pattern_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_php_scrum_user_story';
  $strongarm->value = 1;
  $export['ant_php_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ant_scrum_user_story';
  $strongarm->value = '1';
  $export['ant_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'atrium_activity_update_type_scrum_user_story';
  $strongarm->value = 1;
  $export['atrium_activity_update_type_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_scrum_user_story';
  $strongarm->value = 0;
  $export['comment_anonymous_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_scrum_user_story';
  $strongarm->value = '3';
  $export['comment_controls_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_scrum_user_story';
  $strongarm->value = '3';
  $export['comment_default_mode_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_scrum_user_story';
  $strongarm->value = '2';
  $export['comment_default_order_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_scrum_user_story';
  $strongarm->value = '50';
  $export['comment_default_per_page_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_scrum_user_story';
  $strongarm->value = '1';
  $export['comment_form_location_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_scrum_user_story';
  $strongarm->value = '0';
  $export['comment_preview_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_scrum_user_story';
  $strongarm->value = '2';
  $export['comment_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_scrum_user_story';
  $strongarm->value = '0';
  $export['comment_subject_field_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_scrum_user_story';
  $strongarm->value = array(
    'title' => '37',
    'revision_information' => '47',
    'author' => '46',
    'options' => '48',
    'comment_settings' => '50',
    'menu' => '36',
    'taxonomy' => '35',
    'book' => '45',
    'path' => '49',
    'attachments' => '51',
    'og_nodeapi' => '44',
  );
  $export['content_extra_weights_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_scrum_user_story';
  $strongarm->value = 1;
  $export['enable_revisions_page_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_collapse_scrum_user_story';
  $strongarm->value = '2';
  $export['itweak_upload_collapse_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'modalframe_exclude_node_scrum_user_story';
  $strongarm->value = array(
    0 => 'author',
    1 => 'options',
    2 => 'book',
    3 => 'comment_settings',
    4 => 'menu',
    5 => 'path',
  );
  $export['modalframe_exclude_node_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeformscols_field_placements_scrum_user_story_default';
  $strongarm->value = array(
    'title' => array(
      'region' => 'main',
      'weight' => '0',
      'hidden' => 1,
    ),
    'menu' => array(
      'region' => 'right',
      'weight' => '5',
      'has_required' => FALSE,
      'title' => 'Menu settings',
      'hidden' => 1,
      'collapsed' => 1,
    ),
    'revision_information' => array(
      'region' => 'right',
      'weight' => '7',
      'has_required' => FALSE,
      'title' => 'Revision information',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'comment_settings' => array(
      'region' => 'right',
      'weight' => '10',
      'has_required' => FALSE,
      'title' => 'Comment settings',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'path' => array(
      'region' => 'right',
      'weight' => '9',
      'has_required' => FALSE,
      'title' => 'URL path settings',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'options' => array(
      'region' => 'right',
      'weight' => '8',
      'has_required' => FALSE,
      'title' => 'Publishing options',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'author' => array(
      'region' => 'right',
      'weight' => '6',
      'has_required' => FALSE,
      'title' => 'Authoring information',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'buttons' => array(
      'region' => 'right',
      'weight' => '2',
      'has_required' => FALSE,
      'title' => NULL,
      'hidden' => 0,
    ),
    'book' => array(
      'region' => 'right',
      'weight' => '11',
      'has_required' => FALSE,
      'title' => 'Book outline',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'attachments' => array(
      'region' => 'main',
      'weight' => '8',
      'has_required' => FALSE,
      'title' => 'File attachments',
      'hidden' => 0,
      'collapsed' => 1,
    ),
    'field_scrum_stories_effort' => array(
      'region' => 'right',
      'weight' => '0',
      'has_required' => FALSE,
      'title' => 'Estimated Effort Remaining',
      'hidden' => 0,
    ),
    'field_scrum_stories_iwant' => array(
      'region' => 'main',
      'weight' => '2',
      'has_required' => TRUE,
      'title' => 'I want to be able to',
    ),
    'field_scrum_stories_sothat' => array(
      'region' => 'main',
      'weight' => '3',
      'has_required' => TRUE,
      'title' => 'So that I can',
    ),
    'field_scrum_stories_parent' => array(
      'region' => 'right',
      'weight' => '4',
      'has_required' => TRUE,
      'title' => 'Parent User Story',
      'hidden' => 1,
    ),
    'field_scrum_stories_children' => array(
      'region' => 'main',
      'weight' => '5',
      'has_required' => FALSE,
      'title' => 'Child Stories &amp; Tasks',
      'hidden' => 1,
    ),
    'field_scrum_sprint_reference' => array(
      'region' => 'right',
      'weight' => '1',
      'has_required' => FALSE,
      'title' => 'Sprint',
      'hidden' => 0,
    ),
    'field_scrum_stories_number' => array(
      'region' => 'main',
      'weight' => '7',
      'has_required' => FALSE,
      'title' => NULL,
      'hidden' => 1,
    ),
    'og_nodeapi' => array(
      'region' => 'main',
      'weight' => '6',
      'has_required' => FALSE,
      'title' => 'Groups',
      'collapsed' => 0,
      'hidden' => 1,
    ),
    'notifications' => array(
      'region' => 'right',
      'weight' => '3',
      'has_required' => FALSE,
      'title' => 'Notifications',
      'collapsed' => 0,
      'hidden' => 1,
    ),
    'field_scrum_backlog_priority' => array(
      'region' => 'main',
      'weight' => '9',
      'has_required' => FALSE,
      'title' => 'Backlog Priority',
      'hidden' => 1,
    ),
    'field_scrum_stories_notes' => array(
      'region' => 'main',
      'weight' => '4',
      'has_required' => FALSE,
      'title' => 'Notes',
      'hidden' => 0,
    ),
    'taxonomy' => array(
      'region' => 'main',
      'weight' => '1',
      'has_required' => TRUE,
      'title' => NULL,
    ),
  );
  $export['nodeformscols_field_placements_scrum_user_story_default'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_scrum_user_story';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
    2 => 'revision',
  );
  $export['node_options_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'og_content_type_usage_scrum_user_story';
  $strongarm->value = 'group_post_standard';
  $export['og_content_type_usage_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_scrum_user_story';
  $strongarm->value = 0;
  $export['show_diff_inline_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_scrum_user_story';
  $strongarm->value = 0;
  $export['show_preview_changes_scrum_user_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'taxonomy_override_selector';
  $strongarm->value = TRUE;
  $export['taxonomy_override_selector'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_scrum_user_story';
  $strongarm->value = '1';
  $export['upload_scrum_user_story'] = $strongarm;

  return $export;
}
