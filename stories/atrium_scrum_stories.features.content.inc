<?php

/**
 * Implementation of hook_content_default_fields().
 */
function atrium_scrum_stories_content_default_fields() {
  $fields = array();

  // Exported field: field_scrum_sprint_reference
  $fields['scrum_user_story-field_scrum_sprint_reference'] = array(
    'field_name' => 'field_scrum_sprint_reference',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '43',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'sprint' => 'sprint',
      'blog' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'daily_scrum' => 0,
      'event' => 0,
      'group' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'scrum_task' => 0,
      'scrum_user_story' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => 'atrium_scrum_sprint_selection',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => '',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Sprint',
      'weight' => '43',
      'description' => 'The Sprint that this User Story is assigned to.',
      'type' => 'nodereference_select',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_scrum_stories_children
  $fields['scrum_user_story-field_scrum_stories_children'] = array(
    'field_name' => 'field_scrum_stories_children',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '42',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '0',
    'multiple' => '1',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'scrum_task' => 'scrum_task',
      'scrum_user_story' => 'scrum_user_story',
      'blog' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'daily_scrum' => 0,
      'event' => 0,
      'group' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'sprint' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_scrum_stories_children][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Child Stories & Tasks',
      'weight' => '42',
      'description' => 'The tasks and more detailed, child User Stories that this User Story depends on.',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_scrum_stories_effort
  $fields['scrum_user_story-field_scrum_stories_effort'] = array(
    'field_name' => 'field_scrum_stories_effort',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '38',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'number_integer',
    'required' => '0',
    'multiple' => '0',
    'module' => 'number',
    'active' => '1',
    'prefix' => '',
    'suffix' => ' hrs.',
    'min' => '',
    'max' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scrum_stories_effort][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Estimated Effort Remaining',
      'weight' => '38',
      'description' => 'Estimate the amount of effort required to complete this user story. Do not include effort already accounted for in child user stories or tasks.',
      'type' => 'number',
      'module' => 'number',
    ),
  );

  // Exported field: field_scrum_stories_iwant
  $fields['scrum_user_story-field_scrum_stories_iwant'] = array(
    'field_name' => 'field_scrum_stories_iwant',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '39',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '1',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scrum_stories_iwant][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'I want to be able to',
      'weight' => '39',
      'description' => 'The goal of the User Story. This field is used as the title of User Stories.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_scrum_stories_notes
  $fields['scrum_user_story-field_scrum_stories_notes'] = array(
    'field_name' => 'field_scrum_stories_notes',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '54',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => '5',
      'size' => 60,
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scrum_stories_notes][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Notes',
      'weight' => '54',
      'description' => 'Additional notes that help to elucidate context, list resources and documentation, &c.',
      'type' => 'text_textarea',
      'module' => 'text',
    ),
  );

  // Exported field: field_scrum_stories_parent
  $fields['scrum_user_story-field_scrum_stories_parent'] = array(
    'field_name' => 'field_scrum_stories_parent',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '41',
      'parent' => '',
      'label' => array(
        'format' => 'above',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'nodereference',
    'required' => '1',
    'multiple' => '0',
    'module' => 'nodereference',
    'active' => '1',
    'referenceable_types' => array(
      'scrum_user_story' => 'scrum_user_story',
      'blog' => 0,
      'book' => 0,
      'casetracker_basic_case' => 0,
      'daily_scrum' => 0,
      'event' => 0,
      'group' => 0,
      'profile' => 0,
      'casetracker_basic_project' => 0,
      'shoutbox' => 0,
      'sprint' => 0,
      'scrum_task' => 0,
      'feed_ical_item' => 0,
      'feed_ical' => 0,
    ),
    'advanced_view' => '--',
    'advanced_view_args' => '',
    'widget' => array(
      'autocomplete_match' => 'contains',
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'nid' => NULL,
          '_error_element' => 'default_value_widget][field_scrum_stories_parent][0][nid][nid',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'Parent User Story',
      'weight' => '41',
      'description' => 'If this User Story is logically a component of another User Story, that parent story will be listed here. This field should be read-only in the node-edit form. Parent relationships between User Stories should be set on the Product Backlog page, using the drag-and-drop interface.',
      'type' => 'nodereference_autocomplete',
      'module' => 'nodereference',
    ),
  );

  // Exported field: field_scrum_stories_sothat
  $fields['scrum_user_story-field_scrum_stories_sothat'] = array(
    'field_name' => 'field_scrum_stories_sothat',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '40',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'text',
    'required' => '0',
    'multiple' => '0',
    'module' => 'text',
    'active' => '1',
    'text_processing' => '0',
    'max_length' => '',
    'allowed_values' => '',
    'allowed_values_php' => '',
    'widget' => array(
      'rows' => 5,
      'size' => '60',
      'default_value' => array(
        '0' => array(
          'value' => '',
          '_error_element' => 'default_value_widget][field_scrum_stories_sothat][0][value',
        ),
      ),
      'default_value_php' => NULL,
      'label' => 'So that I can',
      'weight' => '40',
      'description' => 'The benefit that this User Story is likely to provide.',
      'type' => 'text_textfield',
      'module' => 'text',
    ),
  );

  // Exported field: field_scrum_stories_statement
  $fields['scrum_user_story-field_scrum_stories_statement'] = array(
    'field_name' => 'field_scrum_stories_statement',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '34',
      'parent' => '',
      'label' => array(
        'format' => 'hidden',
      ),
      'teaser' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'computed',
    'required' => '0',
    'multiple' => '0',
    'module' => 'computed_field',
    'active' => '1',
    'code' => '$node_field[0][\'value\'] = "";',
    'display_format' => '$display = $node_field_item[\'value\'];',
    'store' => 1,
    'data_type' => 'longtext',
    'data_length' => '',
    'data_not_NULL' => 0,
    'data_default' => '',
    'data_sortable' => 1,
    'widget' => array(
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'User Story Statement',
      'weight' => '34',
      'description' => '',
      'type' => 'computed',
      'module' => 'computed_field',
    ),
  );

  // Exported field: field_scrum_stories_total_effort
  $fields['scrum_user_story-field_scrum_stories_total_effort'] = array(
    'field_name' => 'field_scrum_stories_total_effort',
    'type_name' => 'scrum_user_story',
    'display_settings' => array(
      'weight' => '52',
      'parent' => '',
      'label' => array(
        'format' => 'inline',
      ),
      'teaser' => array(
        'format' => 'computed_value',
        'exclude' => 0,
      ),
      'full' => array(
        'format' => 'computed_value',
        'exclude' => 1,
      ),
      '5' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '4' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '2' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      '3' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
      'token' => array(
        'format' => 'default',
        'exclude' => 0,
      ),
    ),
    'widget_active' => '1',
    'type' => 'computed',
    'required' => '0',
    'multiple' => '0',
    'module' => 'computed_field',
    'active' => '1',
    'code' => '$node_field[0][\'value\'] = "";',
    'display_format' => '$display = $node_field_item[\'value\'];',
    'store' => 1,
    'data_type' => 'int',
    'data_length' => '8',
    'data_not_NULL' => 0,
    'data_default' => '',
    'data_sortable' => 1,
    'widget' => array(
      'default_value' => NULL,
      'default_value_php' => NULL,
      'label' => 'Total Effort Remaining',
      'weight' => '52',
      'description' => '',
      'type' => 'computed',
      'module' => 'computed_field',
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Child Stories & Tasks');
  t('Estimated Effort Remaining');
  t('I want to be able to');
  t('Notes');
  t('Parent User Story');
  t('So that I can');
  t('Sprint');
  t('Total Effort Remaining');
  t('User Story Statement');

  return $fields;
}
