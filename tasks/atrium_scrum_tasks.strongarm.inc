<?php

/**
 * Implementation of hook_strongarm().
 */
function atrium_scrum_tasks_strongarm() {
  $export = array();
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'atrium_activity_update_type_scrum_task';
  $strongarm->value = 1;

  $export['atrium_activity_update_type_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_scrum_task';
  $strongarm->value = 0;

  $export['comment_anonymous_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_scrum_task';
  $strongarm->value = '3';

  $export['comment_controls_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_scrum_task';
  $strongarm->value = '4';

  $export['comment_default_mode_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_scrum_task';
  $strongarm->value = '2';

  $export['comment_default_order_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_scrum_task';
  $strongarm->value = '50';

  $export['comment_default_per_page_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:enforce_new_revision';
  $strongarm->value = 1;

  $export['comment_driven:enforce_new_revision'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:inspect_links';
  $strongarm->value = 1;

  $export['comment_driven:inspect_links'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:node_form_bellow';
  $strongarm->value = 0;

  $export['comment_driven:node_form_bellow'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:theme_support';
  $strongarm->value = 'atrium';

  $export['comment_driven:theme_support'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:type:scrum_task:driven_props';
  $strongarm->value = array(
    'enabled' => 1,
    'cck' => array(
      'field_scrum_stories_effort' => array(
        'enabled' => 1,
      ),
      'field_scrum_stories_parent' => array(
        'enabled' => 0,
      ),
      'field_scrum_tasks_description' => array(
        'enabled' => 0,
      ),
      'field_scrum_tasks_assigned_to' => array(
        'enabled' => 1,
      ),
      'field_scrum_tasks_status' => array(
        'enabled' => 1,
      ),
      'field_scrum_sprint_reference' => array(
        'enabled' => 0,
      ),
    ),
  );

  $export['comment_driven:type:scrum_task:driven_props'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_driven:type:scrum_task:settings';
  $strongarm->value = array(
    'empty_comment' => 1,
    'fieldset:collapsed' => 0,
    'fieldset:title' => 'Update Task',
    'live_render' => '0',
  );

  $export['comment_driven:type:scrum_task:settings'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_scrum_task';
  $strongarm->value = '1';

  $export['comment_form_location_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_scrum_task';
  $strongarm->value = '0';

  $export['comment_preview_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_scrum_task';
  $strongarm->value = '2';

  $export['comment_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_scrum_task';
  $strongarm->value = '0';

  $export['comment_subject_field_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_scrum_task';
  $strongarm->value = array(
    'title' => '-5',
    'revision_information' => '2',
    'author' => '1',
    'options' => '3',
    'comment_settings' => '5',
    'menu' => '-3',
    'book' => '0',
    'path' => '6',
    'attachments' => '4',
    'og_nodeapi' => '-1',
  );

  $export['content_extra_weights_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_scrum_task';
  $strongarm->value = 1;

  $export['enable_revisions_page_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'itweak_upload_collapse_scrum_task';
  $strongarm->value = '2';

  $export['itweak_upload_collapse_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'modalframe_exclude_node_scrum_task';
  $strongarm->value = array(
    0 => 'author',
    1 => 'options',
    2 => 'book',
    3 => 'comment_settings',
    4 => 'menu',
    5 => 'path',
  );

  $export['modalframe_exclude_node_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_scrum_task';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
    2 => 'revision',
  );

  $export['node_options_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'nodeformscols_field_placements_scrum_task_default';
  $strongarm->value = array(
    'title' => array(
      'region' => 'main',
      'weight' => '0',
      'has_required' => TRUE,
      'title' => 'Name',
    ),
    'menu' => array(
      'region' => 'right',
      'weight' => '4',
      'has_required' => FALSE,
      'title' => 'Menu settings',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'revision_information' => array(
      'region' => 'right',
      'weight' => '6',
      'has_required' => FALSE,
      'title' => 'Revision information',
      'collapsed' => 0,
      'hidden' => 1,
    ),
    'comment_settings' => array(
      'region' => 'main',
      'weight' => '7',
      'has_required' => FALSE,
      'title' => 'Comment settings',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'path' => array(
      'region' => 'right',
      'weight' => '8',
      'has_required' => FALSE,
      'title' => 'URL path settings',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'options' => array(
      'region' => 'right',
      'weight' => '7',
      'has_required' => FALSE,
      'title' => 'Publishing options',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'author' => array(
      'region' => 'right',
      'weight' => '5',
      'has_required' => FALSE,
      'title' => 'Authoring information',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'buttons' => array(
      'region' => 'right',
      'weight' => '9',
      'has_required' => FALSE,
      'title' => NULL,
      'hidden' => 0,
    ),
    'book' => array(
      'region' => 'main',
      'weight' => '5',
      'has_required' => FALSE,
      'title' => 'Book outline',
      'collapsed' => 1,
      'hidden' => 1,
    ),
    'attachments' => array(
      'region' => 'main',
      'weight' => '6',
      'has_required' => FALSE,
      'title' => 'File attachments',
      'collapsed' => 1,
      'hidden' => 0,
    ),
    'og_nodeapi' => array(
      'region' => 'main',
      'weight' => '1',
      'has_required' => FALSE,
      'title' => 'Groups',
      'collapsed' => 0,
      'hidden' => 1,
    ),
    'field_scrum_sprint_reference' => array(
      'region' => 'right',
      'weight' => '3',
      'has_required' => FALSE,
      'title' => 'Sprint',
      'hidden' => 0,
    ),
    'field_scrum_stories_effort' => array(
      'region' => 'right',
      'weight' => '0',
      'has_required' => FALSE,
      'title' => 'Estimated Effort Remaining',
      'hidden' => 0,
    ),
    'field_scrum_stories_parent' => array(
      'region' => 'main',
      'weight' => '4',
      'has_required' => TRUE,
      'title' => 'Parent User Story',
      'hidden' => 0,
    ),
    'notifications' => array(
      'region' => 'main',
      'weight' => '2',
      'has_required' => FALSE,
      'title' => 'Notifications',
      'collapsed' => 0,
      'hidden' => 1,
    ),
    'field_scrum_tasks_description' => array(
      'region' => 'main',
      'weight' => '3',
      'has_required' => FALSE,
      'title' => 'Description',
      'hidden' => 0,
    ),
    'field_scrum_tasks_assigned_to' => array(
      'region' => 'right',
      'weight' => '2',
      'has_required' => FALSE,
      'title' => 'Assigned to',
      'hidden' => 0,
    ),
    'field_scrum_tasks_status' => array(
      'region' => 'right',
      'weight' => '1',
      'has_required' => TRUE,
      'title' => 'Status',
    ),
  );

  $export['nodeformscols_field_placements_scrum_task_default'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'og_content_type_usage_scrum_task';
  $strongarm->value = 'group_post_standard';

  $export['og_content_type_usage_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_scrum_task';
  $strongarm->value = 0;

  $export['show_diff_inline_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_scrum_task';
  $strongarm->value = 0;

  $export['show_preview_changes_scrum_task'] = $strongarm;
  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_scrum_task';
  $strongarm->value = '1';

  $export['upload_scrum_task'] = $strongarm;
  return $export;
}
