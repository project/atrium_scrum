<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function atrium_scrum_tasks_context_default_contexts() {
  $export = array();
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_scrum_taskboard';
  $context->description = 'Make the taskboard full page width';
  $context->tag = 'Atrium Scrum';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'scrum_tasks_task_board:page_1' => 'scrum_tasks_task_board:page_1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(),
      'layout' => 'wide',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Atrium Scrum');
  t('Make the taskboard full page width');

  $export['atrium_scrum_taskboard'] = $context;
  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'atrium_scrum_tasks';
  $context->description = 'Adds \'Add Task\' buttons';
  $context->tag = 'Atrium Scrum';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'scrum_task' => 'scrum_task',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'views' => array(
      'values' => array(
        'atrium_scrum' => 'atrium_scrum',
        'atrium_scrum_backlog' => 'atrium_scrum_backlog',
        'atrium_scrum_burndown' => 'atrium_scrum_burndown',
        'atrium_scrum_sprint' => 'atrium_scrum_sprint',
        'scrum_tasks_task_board' => 'scrum_tasks_task_board',
      ),
    ),
  );
  $context->reactions = array(
    'menu' => 'scrum',
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Adds \'Add Task\' buttons');
  t('Atrium Scrum');

  $export['atrium_scrum_tasks'] = $context;
  return $export;
}
