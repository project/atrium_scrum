<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function atrium_scrum_tasks_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "corresponding_node_references" && $api == "default_corresponding_node_references_presets") {
    return array("version" => 1);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function atrium_scrum_tasks_node_info() {
  $items = array(
    'scrum_task' => array(
      'name' => t('Task'),
      'module' => 'features',
      'description' => t('A task required to satisfy a user story.'),
      'has_title' => '1',
      'title_label' => t('Name'),
      'has_body' => '0',
      'body_label' => '',
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function atrium_scrum_tasks_views_api() {
  return array(
    'api' => '2',
  );
}
