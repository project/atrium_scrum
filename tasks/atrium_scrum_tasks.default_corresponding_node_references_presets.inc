<?php

/**
 * Implementation of hook_default_corresponding_node_references_preset().
 */
function atrium_scrum_tasks_default_corresponding_node_references_preset() {
  $export = array();
  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'scrum_task*field_scrum_sprint_reference*sprint*field_scrum_sprint_backlog';
  $preset->enabled = 1;

  $export['scrum_task*field_scrum_sprint_reference*sprint*field_scrum_sprint_backlog'] = $preset;
  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'scrum_task*field_scrum_stories_parent*scrum_user_story*field_scrum_stories_children';
  $preset->enabled = 1;

  $export['scrum_task*field_scrum_stories_parent*scrum_user_story*field_scrum_stories_children'] = $preset;
  $preset = new stdClass;
  $preset->disabled = FALSE; /* Edit this to true to make a default preset disabled initially */
  $preset->api_version = 1;
  $preset->node_types_content_fields = 'scrum_user_story*field_scrum_stories_children*scrum_task*field_scrum_stories_parent';
  $preset->enabled = 1;

  $export['scrum_user_story*field_scrum_stories_children*scrum_task*field_scrum_stories_parent'] = $preset;
  return $export;
}
